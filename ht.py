__author__ = 'Roelof Swanepoel'

import requests
import csv
import requests
import time
import smtplib
from email.mime.text import MIMEText

# API Endpoint
API_ENDPOINT = "https://haveibeenpwned.com/api/v2/"
API_SERVICE = {'account': "breachedaccount", \
                'sites': "breaches", \
                'site': "breach", \
                'data': "dataclasses", \
                'paste': "pasteaccount", \
                'password': "pwnedpassword"}

# E-mail configuration
MAIL_BREACHES = False # Set to True to email breaches - make sure settings below are correct
MAIL_SERVER_SMTP = 'smtp.gmail.com' # SMTP mail server to use
MAIL_SERVER_PORT = 587 # SMTP server port to use
MAIL_SERVER_SSL = False # Set to True if SSL is required, False otherwise
MAIL_SERVER_TLS = True # Set to True if TLS is required, False otherwise
MAIL_SERVER_USER = 'my_name@gmail.com' # The mail server username. If MAIL_SERVER_PASS is blank, no login will be performed.
MAIL_SERVER_PASS = 'my_pass' # The mail server password.  If blank, no login will be performed.
MAIL_SENDER = 'MyName <my_name@gmail.com>' # Who the e-mail will appear to be from
MAIL_SUBJECT = 'New e-mail and password breaches' # The subject line
# The MAIL_BODY contains the introductory text, with breaches listed after that
MAIL_BODY = 'Hi,\n\n\
Your e-mail address has shown up in new breaches below, as reported by Have I Been Pwned (https://haveibeenpwned.com/).\n\
It is important that you immediately change the passwords on these services, as well as any other services where \
you may have used this e-mail and password combination.\n\n'
# The MAIL_FOOTER is included after breaches have been listed
MAIL_FOOTER = '\nKind regards,\n\
Me'


def get_breaches_api(account_id, unverified):
    breaches=[]
    resp = requests.get(API_ENDPOINT+API_SERVICE['account']+'/'+account_id+'?includeUnverified='+unverified)
    if resp.status_code == 200:
        for item in resp.json():
            breaches.append(item)
    else:
       breaches.append({})
    return breaches

def import_csv_breaches(csvFileName):
    accounts={}
    with open(csvFileName, 'rb') as csvfile:
        rawdata = csv.reader(csvfile, delimiter=",")
        for account in rawdata:
            accounts[account[0]] = account[1:]
    return accounts

def get_current_breaches(accounts):
    current_breaches = {}
    for account in accounts:
        current_breaches[account] = get_breaches_api(account,'true')
        time.sleep(2)
    return current_breaches

def find_new_breaches(accounts,current_breaches):
    new_breaches = {}
    new_breach_titles = {}
    for account,existing_breaches in accounts.items():
        new_breaches[account] = []
        new_breach_titles[account] = []
        for breach in current_breaches[account]:
            if 'Name' in breach.keys():
                if breach['Name'] not in existing_breaches:
                    new_breaches[account].append(breach['Name'])
                    new_breach_titles[account].append(breach['Title'])
    return new_breaches, new_breach_titles

def store_new_breaches(new_breaches,new_breaches_name):
    with open(new_breaches_name, 'ab') as csvfile:
        nbwriter = csv.writer(csvfile, delimiter=',')
        for account,breaches in new_breaches.items():
            if len(new_breaches[account]) > 0:
                row = [account]
                for item in new_breaches[account]:
                    row.append(item)
                nbwriter.writerow(row)

def update_accounts(accounts, new_breaches, accounts_file_name):
    with open(accounts_file_name, 'wb') as csvfile:
        accwriter = csv.writer(csvfile, delimiter=',')
        for account,breaches in accounts.items():
            row = [account]
            for item in accounts[account]:
                row.append(item)
            for item in new_breaches[account]:
                row.append(item)
            accwriter.writerow(row)

def mail_new_breaches(new_breach_titles):
    for account,breach_titles in new_breach_titles.items():
        if len(breach_titles) > 0:
            message = MAIL_BODY
            for item in breach_titles:
                message = message + item + '\n'
            message = message + MAIL_FOOTER
            message = MIMEText(message)
            message['Subject'] = MAIL_SUBJECT
            message['From'] = MAIL_SENDER
            message['To'] = account
            if MAIL_SERVER_SSL:
                s = smtplib.SMTP_SSL(MAIL_SERVER_SMTP, MAIL_SERVER_PORT)
                s.ehlo()
            elif MAIL_SERVER_TLS:
                s = smtplib.SMTP(MAIL_SERVER_SMTP, MAIL_SERVER_PORT)
                s.ehlo()
                s.starttls()
            else:
                s = smtplib.SMTP(MAIL_SERVER_SMTP, MAIL_SERVER_PORT)
                s.ehlo()
            if len(MAIL_SERVER_PASS) > 0:
                s.login(MAIL_SERVER_USER, MAIL_SERVER_PASS)
            s.sendmail(MAIL_SENDER, [account], message.as_string())
            s.quit()

accounts = import_csv_breaches('accounts.csv')
current_breaches = get_current_breaches(accounts)
new_breaches, new_breach_titles = find_new_breaches(accounts,current_breaches)
store_new_breaches(new_breaches,'new_breaches.csv')
update_accounts(accounts, new_breaches, 'accounts.csv')
if MAIL_BREACHES:
    mail_new_breaches(new_breach_titles)

