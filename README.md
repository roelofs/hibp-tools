# HIBP Tools

## Introduction
This project is meant to provide a commandline (CLI) cross-platform interface to the [Have I Been Pwned](https://haveibeenpwned.com) service, through a python script.  It was originally written as an entry to [Troy Hunt's](https://troyhunt.com/) HIBP Lenovo competition.  Thanks to Troy and HIBP for a great service!

## Features (Current)
At the moment, the script will read in a CSV file with account names, and check these e-mail addresses against the HIBP service.  If a new leak is found, these will be stored in a seperate file, and the main file updated.

If the correct settings have been updated (see below), the affected accounts will also be emailed a summary of breaches that their e-mail address has been found in.  The idea is that people who are not as informed about online security breaches, are more likely to pay attention to an e-mail sent from someone they know (be it the family IT guy, office system admin, etc).

Any subsequent queries, will only display (and e-mail) new breaches.

The service can also be integrated into the commandline (where we all live, right?), by running a cron job, and checking for the existence of the new breaches file (see below), and updating your MOTD, prompt, login message, etc.

## Features (Future)
* E-mail a summary of recent account breaches to an admin account
* Check for breaches on specified domains
* Add commandline ability to get more information on specific breaches (linked to API "Name" field)
* Add a setup script
* Add a configuration file
* Update documentation for Windows and OSX integration
* Make the project installable via `pip`
* More visible error reporting (in case `cron` job fails, etc)

## Installation
The script is a python script, which means it should run on most any system with a system installation of Python on it (provided the `requests` module can be installed - see below).

It can be installed by doing a `git clone` in the appropriate directory.

It requires Python 2.7 (3.0+ has not been tested yet, but should work).  If the `requests` module hasn't been installed, or gives an error, it can be installed with `pip install requests`.

Before running the script, check the following items:

### Accounts file
In the installation folder, the file `accounts.csv` needs to be updated with e-mail addresses to check.  You can add as many as you want (yours only, family and friends, employees, etc).  One address to a line.

Over time, the file will become populated with additional information in CSV format, but new addresses can still be added at the end.

### E-mail settings
At the top of `ht.py`, you can update the relevant e-mail settings, to automatically e-mail accounts that have been flagged as breached.  Note that only new breaches will be mailed, so running the script once, updating mail settings, and then running again, will only email breaches that have been added since the previous run.

While you can use any email service (or even your own SMTP server), the current e-mail settings have been set up to work with Gmail.  There are two reasons for this:
1. You don't have to ask anyone to add your custom domain to their spam filter whitelist
2. Most people have a gmail address kicking around, and can use it for this

Two issues that may come up with gmail and python scripts:
1. Make sure your gmail account is set up to allow insecure clients: https://myaccount.google.com/lesssecureapps
2. If you receive authorisation errors even with 1 set up, try this link: https://accounts.google.com/DisplayUnlockCaptcha

It may be worthwhile to run the script with one address from the commandline first, to make sure that it works correctly.

If you want to to use an account other than gmail, the comments in `ht.py` should be enough to set you up.

### Cron job
If you want the script to run at regular intervals (once daily, once weekly, etc), add a cron job in any \*nix based system.  On Ubuntu, this can be performed with the `crontab -e` command.  Commands inserted here, will run as that user (so you don't need superuser privileges).  The sample entry below, will run the script every day at noon, and outputs will be stored in the `hibp-tools` installation directory.

```
# m h  dom mon dow   command
00 12 * * * cd ~/hibp-tools; python ht.py
```

### Notifications
By checking for the existence of the `new_breaches.csv` file (and size > 0) in the installation folder, you can set up an appropriate alert in your commandline.  Good uses would be a message during login, or a modified prompt.

#### Ubuntu notifications example
On Ubuntu, modify your `~/.bashrc` file, by adding the following line.  The section below `# HIBP Prompt notification` will modify your prompt, while `# HIBP Login notification` will display a message.  Apart from the first line, the two sections are independant - keep or remove whichever is useful.  Make sure that `HIBP_NOTIFY` points to the correct installation directory.

```bash
HIBP_NOTIFY="$HOME/hibp-tools/new_breaches.csv"

# HIBP Prompt notification
DEFAULT_PROMPT=$PS1
if [ -s $HIBP_NOTIFY ]; then
    PS1='[NEW HIBP BREACHES] '$DEFAULT_PROMPT
fi

# HIBP Login notification
if [ -s $HIBP_NOTIFY ]; then
    NUM_BREACHES=$(wc -l < $HIBP_NOTIFY)
    printf "\n -- New HIBP Notifications Available! --\n\n"
    printf "There are currently $NUM_BREACHES new entries in $HIBP_NOTIFY.\n\n"
    printf "To clear this notification, rename or delete $HIBP_NOTIFY.\n\n"
fi
```

If the `new_breaches.csv` file exists and is non-zero in size in the directory specified, after a new login the following will be displayed:

>>>
 -- New HIBP Notifications Available! --

 There are currently 2 new entries in /home/user/hibp-tools/new_breaches.csv.

 To clear this notification, rename or delete /home/user/hibp-tools/new_breaches.csv.

[NEW HIBP BREACHES] user@host:~$
>>>

## Usage
1. Perform any necessary installation items as above.
2. Run `python ht.py` from the installation folder (either directly, or via cron job).
3. Any new leaks will be listed in `new_breaches.csv`, and `accounts.csv` updated with these breaches as well.  This stops subsequent runs of the script from displaying the same information.
4. Removing `new_breaches.csv` will (if it has been set up) remove any notifications.  This step is intentionally manual, since it will allow you to tend to new information at your own time, but still be reminded of it.
5. To reset the 'database', simply remove any items after the account name in `accounts.csv`

## Questions/Suggestions
Please fee free to contact me here, or at roelof.swanepoel@gmail.com
